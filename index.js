// Bài 1
/*
*input ; Số Tiền Lương 1 ngày : 100 000

*Các bước xử lý :
-tạo biến "luongNgay" "soNgay" "tongLuong"
-gán giá trị cho "luongNgay"                                      100000
-gán giá trị cho  "soNgay"                                        30
-Sử dụng công thức;   tongLuong = luongNgay * soNgay
-in ra kết quả console

*output ; tổng tiềng lương nhận được 30000000

*/

var luongNgay = 100000;

var soNgay = 30;

var tongLuong = luongNgay * soNgay;

console.log( "Tổng tiền lương nhận được là :", tongLuong);

// bài 2
/*
*input : cho 5 số thực bất kì 4 2 6 5 8

*các bước xử lý:
-tạo biến "num1" "num2" "num3" "num4" "num5" "numTb"
-gán giá trị cho "num1" "num2" "num3" "num4" "num5"
-sử dụng công thức : "numTb" = ("num1" + "num2" + "num3" + "num4" + "num5") / 5
-ỉn ra kết quả console

*output : giá trị trung bình là : 384

*/

var num1 = 4;
var num2 = 2;
var num3 = 6;
var num4 = 5;
var num5 = 8;

var numTb = (num1 * num2 * num3 * num4 * num5) / 5

console.log("Giá trị trung bình là :" , numTb)


// bài 3
/*
*input ; giá 1 USD là 23000

*Các bước thực hiện
-tạo biến "giaUSD" "soUSD" "soVND"
-gán giá trị cho "giaUSD"
-gán giá trị cho "soUSD"
-tính theo công thức ; "soVND" = "giaUSD" * "soUSD"
-ỉn ra kết quả console

*output  số tiền sau khi quy đổi là  2350000

*/
var giaUSD = 23500;

var soUSD = 100 ;

var soVND = giaUSD * soUSD

console.log("số tiền sau khi quy dổi là :" , soVND)

// bài 4
/*
*input ; Cho chièu dài và chiều rộng của hình chữ nhật  4, 5

*các bước thực hiện
-tạo biến "longs" "withs" "area" "perimeter"
-gán giá trị cho "longs" "with"
-tính diện tích vsf chu vi theo công thức 
+  diện tích  "area" = " longs" * "withs"
+ chu vi      "perimeter" = ("longs" + "withs") * 2

-in ra kết quả console

*output diện tích hình chữ nhật là 20
        chu vi hình chữ nhật là 18

*/

var longs = 5;
var withs = 4;

var area = longs * withs;

var perimeter = (longs + withs) * 2;

console.log("Diện tích hình chữ nhật là :", area ,  " ; Chu vi hình chữu nhật là : " , perimeter);


// bài 5
/*
*input: cho số có 2 chữ số 35

*Các bước thực hiện
-tạo biến "num" "num1" "num2" "total"
-gán giá trị cho "num"
-tìm "num1" bằng cách ; "num1" = Math.floor("num" / 10)
-tìm "num2" bằng cách ; "num2" = "num" % 10
-tìm "total" bằng cách "total" = "num1" + "num2"
-in ra két quả console

*output tổng ký số là 8

*/

var num = 35;

var num1 = Math.floor(35/10);

var num2 = 35 % 10 ;


var total = num1 + num2 ;

console.log("Tổng ký số là :", total);










